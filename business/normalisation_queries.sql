insert into db_grad_cs_1917.instrument (instrument_name)
(select distinct instrument_name from db_grad.deal)
;


insert into db_grad_cs_1917.counterparty (counterparty_name, counterparty_status, counterparty_date_registered)
select distinct counterparty_name,counterparty_status,counterparty_date_registered from db_grad.deal ;


insert into db_grad_cs_1917.deal (deal_id, deal_counterparty_id, deal_instrument_id, deal_type, deal_amount, deal_quantity)
select d.deal_id, 
(select counterparty_id from db_grad_cs_1917.counterparty c where c.counterparty_name = d.counterparty_name),
(select instrument_id from db_grad_cs_1917.instrument i where i.instrument_name = d.instrument_name),
d.deal_type, d.deal_amount,d.deal_quantity from db_grad.deal d

