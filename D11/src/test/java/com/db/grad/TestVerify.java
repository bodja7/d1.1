package com.db.grad;

import com.db.grad.formatter.JsonFormatter;
import com.db.grad.verification.Verify;
import org.junit.Test;

import junit.framework.TestCase;

public class TestVerify extends TestCase {
	@Test
	public void test_get_json_call_matches_createJson() {

		String username = "selvyn";
		String password = "gradprog2016";

		String JsonFormatterMessageExpected = JsonFormatter.createValidateJson(username, password,true);

		Verify v = new Verify();
		v.performVerification(username, password);
		String VerifyGetJsonResult = v.getJson();

		assertEquals(VerifyGetJsonResult, JsonFormatterMessageExpected);
	}

	@Test
	public void test_verify_perform_verification_valid_credentials(){

		String validUn = "selvyn";
		String validPw = "gradprog2016";

		Verify v = new Verify();

		assertTrue(v.performVerification(validUn, validPw));

	}

	public void test_verify_perform_verification_invalid_credentials(){

		String invalidUn = "678910";
		String invalidPw = "12345";

		Verify v = new Verify();

		assertFalse(v.performVerification(invalidUn, invalidPw));

	}



	@Test
	public void test_verify_username_setter(){

		String expectedUsername = "userOne";
		Verify v = new Verify();
		v.setUsername("userOne");

		assertEquals(v.getUsername(), expectedUsername);
	}

	@Test
	public void test_verify_username_getter(){

		Verify v = new Verify();
		String expectedUsername = "userTwo";
		v.setUsername("userTwo");
		String resultUsername = v.getUsername();

		assertEquals(expectedUsername, resultUsername);
	}

	@Test
	public void test_verify_password_setter(){

		String expectedPassword = "Graduate2018";
		Verify v = new Verify();
		v.setPassword("Graduate2018");

		assertEquals(v.getPassword(), expectedPassword);
	}

	@Test
	public void test_verify_password_getter(){

		Verify v = new Verify();
		String expectedPassword = "dbank";
		v.setUsername("dbank");
		String resultPassword = v.getUsername();

		assertEquals(expectedPassword, resultPassword);
	}




}
