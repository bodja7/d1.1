package com.db.grad.database;

import org.json.JSONArray;
import org.json.JSONObject;


import java.sql.*;

public class TableExtractor {
    private static final String query = "SELECT * from ";
    private static final String avgQuery = "SELECT\n" +
            "  db_grad_cs_1917.instrument.instrument_name,\n" +
            "  Avg(db_grad_cs_1917.deal.deal_amount) AS Avg_deal_amount,\n" +
            "  db_grad_cs_1917.deal.deal_type\n" +
            "FROM\n" +
            "  db_grad_cs_1917.instrument\n" +
            "  INNER JOIN db_grad_cs_1917.deal ON db_grad_cs_1917.deal.deal_instrument_id = db_grad_cs_1917.instrument.instrument_id\n" +
            "WHERE\n" +
            "  db_grad_cs_1917.deal.deal_type = \"s\"\n" +
            "GROUP BY\n" +
            "  db_grad_cs_1917.instrument.instrument_name,\n" +
            "  db_grad_cs_1917.deal.deal_type;";
    private static final String countQuery = "SELECT\n" +
            "  db_grad_cs_1917.counterparty.counterparty_name,\n" +
            "  db_grad_cs_1917.instrument.instrument_name,\n" +
            "  (Sum(case when db_grad_cs_1917.deal.deal_type = 's' then db_grad_cs_1917.deal.deal_quantity else 0 end) -\n" +
            "  Sum(case when db_grad_cs_1917.deal.deal_type = 'b' then db_grad_cs_1917.deal.deal_quantity else 0 end)) AS total\n" +
            "FROM\n" +
            "  db_grad_cs_1917.deal\n" +
            "  INNER JOIN db_grad_cs_1917.instrument ON db_grad_cs_1917.deal.deal_instrument_id =\n" +
            "    db_grad_cs_1917.instrument.instrument_id\n" +
            "  INNER JOIN db_grad_cs_1917.counterparty ON db_grad_cs_1917.deal.deal_counterparty_id =\n" +
            "    db_grad_cs_1917.counterparty.counterparty_id\n" +
            "GROUP BY\n" +
            "  db_grad_cs_1917.counterparty.counterparty_name,\n" +
            "  db_grad_cs_1917.instrument.instrument_name,\n" +
            "  db_grad_cs_1917.deal.deal_type";
    private static Connection con;

    private static void init(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://10.11.32.21:3306/db_grad_cs_1917", "dbgrad", "dbgrad"); // gets a new connection
        }catch (SQLException | ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

    }
    private static void close(){
        try {
            con.close();
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }

    }

    public static String getJson(QueryType queryType){
        init();
        JSONArray jsonArray = new JSONArray();
        try {
            PreparedStatement preparedStatement;
            if (queryType.equals(QueryType.AVG_QUERY)){
                preparedStatement = con.prepareStatement(avgQuery);
            }else {
                preparedStatement = con.prepareStatement(countQuery);
            }
            ResultSet rs = preparedStatement.executeQuery();
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (rs.next()) {
                JSONObject jsonObject = new JSONObject();
                for (int j = 1; j < columnsNumber+1; j++) {
                    jsonObject.put(rsmd.getColumnName(j),rs.getString(j));
                }
                jsonArray.put(jsonObject);
            }
            return jsonArray.toString();
        }catch (SQLException e){
            System.out.println(e.getMessage());
            return jsonArray.toString();
        }
        finally {
            close();
        }
    }
}
