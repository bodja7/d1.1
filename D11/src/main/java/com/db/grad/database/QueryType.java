package com.db.grad.database;

public enum QueryType {
    AVG_QUERY,
    SUM_QUERY
}
