package com.db.grad.database;

import com.db.grad.formatter.JsonFormatter;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBaseService {
    public final int HASH_SIZE = 8;
    private String[] tables = {"anonymous_users","counterparty","deal","instrument","login_trail","users"};
    private final String query = "Select * from ";
    public String getData(String type){
        StringBuilder strBuild = new StringBuilder();
        strBuild.append("<html><head><style>\n" +
                "a {\n" +
                "background-color: #555555;\n" +
                "text-align: center;\n" +
                "color: #ffffff;\n" +
                "padding: 8px;\n" +
                "margin: 2px;\n" +
                "display: block;\n" +
                "text-decoration: none;\n" +
                "width: 15%;\n" +
                "}\n" +
                "table {\n" +
                "    font-family: arial, sans-serif;\n" +
                "    border-collapse: collapse;\n" +
                "    width: 100%;\n" +
                "}\n" +
                "\n" +
                "td, th {\n" +
                "    border: 1px solid #dddddd;\n" +
                "    text-align: left;\n" +
                "    padding: 8px;\n" +
                "}\n" +
                "\n" +
                "tr:nth-child(even) {\n" +
                "    background-color: #dddddd;\n" +
                "}\n" +
                "</style></head><body>");
        int numberOfTable = Integer.valueOf(type) - 1;
        switch(numberOfTable) {
            case 0:
                strBuild.append("<h1>anonymous_users</h1>" +
                        "<a href=\"..\\home.jsp\">Home</a>" +
                        "<br><table>" +
                        "<tr><th>anonymous_user_id</th>" +
                        "<th>anonymous_user_pwd</th></tr>");
                break;
            case 1:
                strBuild.append("<h1>counterparty</h1>" +
                        "<a href=\"..\\home.jsp\">Home</a>" +
                        "<br><table>" +
                        "<tr><th>counterparty_id</th>" +
                        "<th>counteryparty_name</th>" +
                        "<th>counterparty_status</th>" +
                        "<th>counterparty_date_registered</th></tr>");
                break;
            case 2:
                strBuild.append("<h1>deal</h1>" +
                        "<a href=\"..\\home.jsp\">Home</a>" +
                        "<br><table>" +
                        "<tr><th>deal_id</th>" +
                        "<th>deal_counterparty_id</th>" +
                        "<th>deal_instrument_id</th>" +
                        "<th>deal_type</th>" +
                        "<th>deal_amount</th></tr>");
                break;
            case 3:
                strBuild.append("<h1>instrument</h1>" +
                        "<a href=\"..\\home.jsp\">Home</a>" +
                        "<br><table>" +
                        "<tr><th>instrument_id</th>" +
                        "<th>instrument_name</th></tr>");
                break;
            case 4:
                strBuild.append("<h1>login_trail</h1>" +
                        "<a href=\"..\\home.jsp\">Home</a>" +
                        "<br><table>" +
                        "<tr><th>login_id</th>" +
                        "<th>logged_in_user_id</th>" +
                        "<th>logged_in_auser_id</th>" +
                        "<th>login_date_and_time</th></tr>");
                break;
            case 5:
                strBuild.append("<h1>users</h1>" +
                        "<a href=\"..\\home.jsp\">Home</a>" +
                        "<br><table>" +
                        "<tr><th>user_id</th>" +
                        "<th>user_pwd</th></tr>");
        }
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://10.11.32.21:3306/db_grad_cs_1917", "dbgrad", "dbgrad"); // gets a new connection
            PreparedStatement preparedStatement = con.prepareStatement(query+tables[numberOfTable]);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()){
                switch (numberOfTable) {
                    case 0: {
                        strBuild.append("<tr><td>");
                        strBuild.append(rs.getString(1));
                        strBuild.append("</td><td>");
                        strBuild.append(JsonFormatter.hash(rs.getString(2),HASH_SIZE));
                        strBuild.append("</td></tr>");
                        break;
                    }
                    case 1: {
                        strBuild.append("<tr><td>");
                        strBuild.append(rs.getString(1));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(2));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(3));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(4));
                        strBuild.append("</td></tr>");
                        break;
                    }
                    case 2: {
                        strBuild.append("<tr><td>");
                        strBuild.append(rs.getString(1));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(2));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(3));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(4));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(5));
                        strBuild.append("</td></tr>");
                        break;
                    }
                    case 3: {
                        strBuild.append("<tr><td>");
                        strBuild.append(rs.getString(1));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(2));
                        strBuild.append("</td></tr>");
                        break;
                    }
                    case 4: {
                        strBuild.append("<tr><td>");
                        strBuild.append(rs.getString(1));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(2));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(3));
                        strBuild.append("</td><td>");
                        strBuild.append(rs.getString(4));
                        strBuild.append("</td></tr>");
                        break;
                    }
                    case 5: {
                        strBuild.append("<tr><td>");
                        strBuild.append(rs.getString(1));
                        strBuild.append("</td><td>");
                        strBuild.append(JsonFormatter.hash(rs.getString(2),HASH_SIZE));
                        strBuild.append("</td></tr>");
                    }
                }
            }
            strBuild.append("</table></body></html>");
            con.close();
            return new String(strBuild);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
