package com.db.grad.verification;

import com.db.grad.formatter.JsonFormatter;

import java.sql.*;

public class Verify {
	private String username;
	private String password;
	private boolean isVerified;

	public boolean performVerification(String inputUsername, String inputPassword) {
		if(null == inputPassword || null == inputUsername){
		    return false;
        }
	    setUsername(inputUsername);
		setPassword(inputPassword);

        try {
            isVerified = false;
            Class.forName("com.mysql.jdbc.Driver");
            Connection con = DriverManager.getConnection("jdbc:mysql://10.11.32.21:3306/db_grad_cs_1917", "dbgrad", "dbgrad"); // gets a new connection
            PreparedStatement preparedStatement = con.prepareStatement("select * from users where user_id = ? AND user_pwd = ?");
            preparedStatement.setString(1,getUsername());
            preparedStatement.setString(2,getPassword());
            ResultSet rs = preparedStatement.executeQuery();
            rs.last();
            if(rs.getRow() == 1){
                isVerified = true;
            }
            con.close();
            return isVerified;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return false;
        }
	}
	public String getJson(){
	    return JsonFormatter.createValidateJson(username,password,isVerified);
    }

    public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setUsername(String inputUsername) {
		username = inputUsername;
	}
	
	public void setPassword(String inputPassword) {
		password = inputPassword;
	}
}
