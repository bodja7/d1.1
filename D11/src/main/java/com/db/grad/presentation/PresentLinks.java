package com.db.grad.presentation;

import com.db.grad.formatter.JsonFormatter;

import java.util.ArrayList;
import java.util.Arrays;

public class PresentLinks {
    private ArrayList<String> links;

    public PresentLinks(String[] inputLinks) {
        links = new ArrayList<>(Arrays.asList(inputLinks));
    }

    public String returnLinksJson() {
        return JsonFormatter.createLinksJson(links);
    }
}
