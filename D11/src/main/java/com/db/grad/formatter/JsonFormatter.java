package com.db.grad.formatter;

import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class JsonFormatter {

    public static String createValidateJson(String userName, String password, boolean isValid){
        String message;
        JSONObject json = new JSONObject();
        json.put("username", userName);
        json.put("password", hash(password,5));
        json.put("validation", isValid);

        message = json.toString();
        return message;
    }

    public static String createLinksJson(ArrayList<String> links) {
        JSONObject json = new JSONObject();

        for (int i = 0; i < links.size();i++) {
            json.put(String.valueOf(i+1), links.get(i));
        }
        return json.toString();
    }

    public static String hash(String message, int size) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedhash = digest.digest(
                    message.getBytes(StandardCharsets.UTF_8));
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < size; i++) {
                String hex = Integer.toHexString(0xff & encodedhash[i]);
                if(hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }

    }
}
